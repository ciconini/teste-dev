var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var gutil = require('gulp-util');
var cleanCSS = require('gulp-clean-css');


gulp.task('style', function() {
    gulp.src('./src/css/*.css')
    .pipe(concat('style.css'))
    .pipe(cleanCSS({compatibility: 'ie9'}))
    .pipe(gulp.dest('./src'));
});

gulp.task('script', function() {
    gulp.src('./src/js/*.js')
    .pipe(concat('./src'))
    .pipe(rename('script.min.js'))
    .pipe(uglify().on('error',gutil.log))
    .pipe(gulp.dest('./src'));
});

gulp.task('default', ['script', 'style'], function() {
    gulp.watch('./src/css/**.css', ['style']);
    gulp.watch('./src/js/**.js', ['script']);
});