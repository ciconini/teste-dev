$(document).ready(function(){ 
    //BANNER
	$('.control-item').click(function() {
        var id = '.' + $(this).attr('id');
        $('.control-item').removeClass('active');
        $('.banner-bg').fadeOut();
        $(this).addClass('active');
        $(id).fadeIn();
    });

    //CAROUSEL
    var currentPos = $('.slide-list').attr('data-current');
    $('.prev').click(function() {
        var w = $('.slide-item').width()+30; //+30 padding
        if (currentPos > 1) {
            currentPos--;
            w = w*(currentPos-1);
            $('.slide-list').scrollLeft(w);
        }   
        $('.slide-list').attr('data-current',currentPos);
    })
    $('.next').click(function() {
        var w = $('.slide-item').width()+30; //+30 padding
        if (currentPos < 7) {
            w = w*currentPos;
            $('.slide-list').scrollLeft(w);
            currentPos++;
        } 
        $('.slide-list').attr('data-current',currentPos);
    })

    //ACCORDION
    $('.accordion-header').click(function() {
        var c = $(this).attr('data-target');
        if (!$(this).hasClass('active')) {
            $('.accordion-header').removeClass('active');
            $('.accordion-content').slideUp();
            $('.arrow').attr('src','./images/arrow-white.png')
            $(this).addClass('active');
            $('#'+c).slideDown();
            $(this).children('.arrow').attr('src','./images/arrow-dark.png')
        } else {
            $(this).toggleClass('active');
            $('#'+c).slideUp();
            $(this).children('.arrow').attr('src','./images/arrow-white.png')
        }
    })

    //CONTACT
    $('.input-email').mask('A', {
        translation: {
            "A": { pattern: /[\w@\-.+]/, recursive: true }
        }
    });
    $('.input-phone').mask('(00) 0000-00000');
    $('#contact-form').validate({
            rules : {
                nome:{
                    required:true
                },
                email:{
                    required:true
                },
                telefone: {
                    required:true
                },
                mensagem:{
                    required:true
                }                                
            },
            messages:{
                nome:{
                    required:"Por favor, informe seu nome"
                },
                email:{
                    required:"É necessário informar um email"
                },
                telefone:{
                    required:"Por favor, informe seu telefone"
                },
                mensagem:{
                    required:"A mensagem não pode ficar em branco"
                }     
            }
    });
});